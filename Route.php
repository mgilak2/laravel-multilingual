<?php namespace LanguageString;
use Illuminate\Support\Facades\Facade;
/**
 * @see \Illuminate\Routing\Router
 */
class Route extends Facade {

    public static $default_lang = '';

    /**
     * theory behind this method is not jumping over laravel track
     * in this method i only tamper with the arguments and pass it back to
     * its parent (in fact laravel track)
     * @param string $method
     * @param array $args
     * @return mixed
     */
    public static function __callStatic($method, $args)
    {
        // groups has prefix key to be tamperd
        if($method == 'group')
        {
            if(isset($args[0]['prefix']))
            {
                $url = [$args[0]['prefix']];
                self::renderUrl($url);
                $args[0]['prefix'] = $url[0];
            }
            return parent::__callStatic($method,$args);
        }

        self::renderUrl($args);

        return parent::__callStatic($method,$args);
    }

    /**
     * finds specific abbreviation in requested url
     * abbreviation is filled in $langs variable
     */
    public static function default_lang()
    {
        if(self::$default_lang == '')
        {
            $status = false;

            // if you wish to make this abbreviation dynamic fill this variable
            // with your languages ;) its simple enough.
            $langs = ['en','fa','fr'];

            if(isset($_SERVER['REQUEST_URI']))
            {
                $urlParts = explode("/",$_SERVER['REQUEST_URI']);
                foreach($langs as $lang)
                {
                    if(in_array($lang,$urlParts))
                    {
                        self::$default_lang = $lang;
                    }
                }
            }
        }
    }

    /**
     * replace '[m]' with the specified language abbreviation in url in all routes
     * @param $args
     */
    public static function renderUrl(&$args)
    {
        self::default_lang();
    
        $args[0] = str_replace("[m]", self::$default_lang, $args[0]);
    }


    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor() { return 'router'; }

}
