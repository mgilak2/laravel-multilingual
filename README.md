# laravel multilingual 
adding fr , sp and other language abbreviation to your routes dynamically for laravel 4 and 5 

> things like example.com/fr/admin , example.com/sp/some-title-here

i just copied pasted Route facade into another file and added some functions to it 

### Installation
```
composer require gilak/laravel-multilingual
```

laravel 5 -> 
open up config/app.php

laravel 4 -> open up app/config/app.php

find this 

> 'Route'     => 'Illuminate\Support\Facades\Route',

comment it out 
and add this to array

> 'Route' => 'MultiLingual\Route',

### Usage
```php
Route::get("[m]/admin",...);

Route::group(['prefix'=>'[m]/admin'],...);
```
if you use group there is no need for adding [m] sign to its nested routes 

### Code Scenario 
read it . your gonna expande it for yourself defenitly 
here i`m going to explain whats going on 

default_lang() 

in default_lang function i just add some hard coded language abbreviation (you can make it dynamic if you wish) , with the help of REQUEST_URI i check whether there is any defind language in it or not . if it is then i set self::$default_language

renderUrl()

in this function i just replace the '[m]' sign with the language in any route that consists it

__callStatic

this method is sweet enough to handle your multilingual route and pass it back to parent __callStatic method in order to be handled properly  
